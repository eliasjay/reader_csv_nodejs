const mongoose = require("mongoose");
const fileSystem = require("fs");
const csv = require("fast-csv");


URL_BASE = 'ds137483.mlab.com'
PORT = 37483
USERNAME = 'joseelias'
PASSWORD = '1234abc'
DATABASE_NAME = 'csv_test'


function connectDatabase(URL_BASE) {
  var stringConnection = `mongodb://${USERNAME}:${PASSWORD}@${URL_BASE}:${PORT}/${DATABASE_NAME}`;
  console.log('String Connection: ', stringConnection);

  return new Promise((resolve, reject) => {
    mongoose.connect(
      stringConnection
    );
  });
};


function readCsv() {
  csv
    .fromStream(stream, { headers: true })
    .on("data", function (data) {
      myPromise(data);
    })
    .on("end", function () {
      console.log("\ndone");
    });
};


function myPromise(data) {
  return new Promise((resolve, reject) => {
    db.collection("data").insertOne(data, function (err, records) {
      console.log("Row:\n", data, "\n");
    });
  }, err => {
    console.log('Error: ', err);
  })
    .then(function (result) {
      console.log("promise executing...");
    })
    .catch(function (err) {
      console.log(err);
    });
};


const db = mongoose.connection;

var stream = fileSystem.createReadStream("apps.csv");
connectDatabase(URL_BASE)
  .then(
    console.log('Successed Connection')
  )
  .catch((err) => {
    console.log('Error: ', err);
  });
readCsv();
