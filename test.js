// Desenvolver um código para node que:
// -[x] Lê um arquivo CSV
// -[x] Insere cada linha lida no banco de dados
// -[ ] Exibe na tela os registros inseridos no banco

// Fazer uma versão com Promise (then / catch) e outra com async await.
// -[ ] versao promise
// -[ ] versao async await

const mongoose = require("mongoose");
const fs = require("fs");
const csv = require("fast-csv");
// https://www.npmjs.com/package/fast-csv

mongoose.connect(
  "mongodb://joseelias:1234abc@ds137483.mlab.com:37483/csv_test"
);
const db = mongoose.connection;

var stream = fs.createReadStream("apps.csv");

csv
  .fromStream(stream, { headers: true })
  .on("data", function(data) {
    var document = {
      app: data.app,
      category: data.category,
      rating: data.rating,
      reviews: data.reviews,
      size: data.size,
      installs: data.installs,
      version: data.version
    };

    var myPromise = () => {
      return new Promise((resolve, reject) => {
        db.collection("data").insertOne(document, function(err, records) {
          console.log("Row:", data);
        });
      })
        .then(function(result) {
          console.log("ola");
          return result;
        })
        .catch(function(err) {
          console.log(err);
        });
    };

    myPromise()
      .then(res => {
        // res.json(result);
        console.log("result....", res);
      })
      .catch(err => {
        console.log(err);
      });
  })
  .on("end", function() {
    console.log("\ndone");
  });
