Desenvolver um código para node que:
 - [x] Lê um arquivo CSV
 - [x] Insere cada linha lida no banco de dados
 - [x] Exibe na tela os registros inseridos no banco

Fazer uma versão com Promise (then / catch) e outra com async await.
 - [ ] versao promise
 - [ ] versao async await
